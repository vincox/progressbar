var buttons;
var bars;
var limit;

var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
        var status = xhr.status;
        if (status === 200) {
            callback(null, xhr.response);
        } else {
            callback(status, xhr.response);
        }
    };
    xhr.send();
};

window.addEventListener("load", function() {
    getJSON('http://pb-api.herokuapp.com/bars',
        function(err, data) {
            if (err !== null) {
                alert('Something went wrong: ' + err);
            } else {
                buttons = data.buttons;
                bars = data.bars;
                limit = data.limit;

                for (j = 0; j < bars.length; ++j) {
                    generateDropdown("#progress " + (j + 1), "bar" + (j + 1));
                    generateBar(j + 1, bars[j]);
                }

                for (i = 0; i < buttons.length; ++i) {
                    generateButtons(buttons[i], i);
                }
            }
        });
}, false); 

function generateButtons(btnNum, index) {
    var dropdown = document.getElementById("ddlProgress");
    if (dropdown != null) {
        var dropdownValue = dropdown[dropdown.selectedIndex].value;
        var element = document.createElement("button");
        if (parseInt(btnNum) > 0 ){
            element.innerHTML = "+" + btnNum;
        }else{
            element.innerHTML = btnNum;
        }
        
        element.name = "btn" + btnNum;
        element.id = "btn" + index;
        element.className = "btn btn-primary";
        element.increment = btnNum;
        element.bar = dropdownValue;
        element.style.marginRight = "10px";
        element.style.width = "80px";
        element.addEventListener('click', callNanobar, false);

        var buttonsDiv = document.getElementById("buttons");
        buttonsDiv.appendChild(element);
    }
}

function callNanobar(e) {
    var div = document.getElementById(e.target.bar);

    var bar = null;
    var nanoWidth = 0;
    for (var i = 0; i < div.childNodes.length; i++) {
        if (div.childNodes[i].className == "nanobar") {
            bar = div.childNodes[i];
            break;
        }
    }

    var nanobar = null;
    for (var i = 0; i < bar.childNodes.length; i++) {
        if (bar.childNodes[i].className == "bar") {
            nanoWidth = bar.childNodes[i].style.width;
            nanoWidth = nanoWidth.slice(0, -1); //remove the % character
            nanobar = bar.childNodes[i];
            break;
        }
    }

    var progressbar = new Nanobar({
        target: div
    });
    progressbar.go(parseInt(nanoWidth) + parseInt(e.target.increment));
    div.removeChild(bar);
}

function generateDropdown(text, value) {
    var dropdown = document.getElementById("ddlProgress");
    var opt = document.createElement("option");
    opt.text = text;
    opt.value = value;
    if (dropdown != null) {
        dropdown.options.add(opt);
    }
}

function dropdownChanged() {
    var dropdown = document.getElementById("ddlProgress");
    var dropdownValue = dropdown[dropdown.selectedIndex].value;
    i = 0;
    do {
        var btn = document.getElementById("btn" + i);
        btn.bar = dropdownValue;
        i++;
    }
    while (document.getElementById("btn" + i) != null);
}

function generateBar(barId, barPercentage) {
    var div = document.createElement("div");
    div.id = "bar" + barId;
    div.style.paddingBottom = "20px";
    var mybars = document.getElementById("bars");
    if (mybars != null) {
        mybars.appendChild(div);
    }
    var progressbar = new Nanobar({
        target: document.getElementById("bar" + barId)
    });
    progressbar.go(barPercentage);
}