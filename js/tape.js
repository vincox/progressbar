var test = require( 'tape' ) ;
test( 'Buttons Generation Test', function( assert ) {
	buttonsFromJson = 4;//assuming there are 4 buttons from the json response
	buttonsGenerated = 0;
	for (i = 0; i < buttonsFromJson; ++i) {
        buttonsGenerated++;
    }
	assert.equal(buttonsFromJson, buttonsGenerated, 'Buttons can be generated succesfully' ) ;
	assert.end() ;
});
