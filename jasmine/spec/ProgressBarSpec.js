describe("ProgressBar", function() {
	describe('Did AJAX call respond?', function() {
	  var progressBarJson;

	  beforeEach(function(done) {
	    getJSON('http://pb-api.herokuapp.com/bars',
		  function(err, data) {
		    progressBarJson = data;
		    done();
		  });
	  });

	  it('AJAX Call is successful', function(done) {
	    console.log(progressBarJson);
	    expect(progressBarJson).not.toEqual({});
	    expect(progressBarJson).not.toBeUndefined();
	    done();
	  });
	});
});